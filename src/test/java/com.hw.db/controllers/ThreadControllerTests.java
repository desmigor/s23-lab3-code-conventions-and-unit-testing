package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.*;
import com.hw.db.models.Thread;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class ThreadControllerTests {
    private int threadID = 30;
    private String username = "Username";
    private String slug = "Slug";
    private ThreadController controller;
    private User user;
    private Thread thread;
    private Post post;

    @BeforeEach
    void setup() {
        final Timestamp ts = Timestamp.valueOf("2021-02-07 00:00:00");
        final String forum = "theForum";
        user = new User(username, "user@example.com", "name", "Hello World");
        post = new Post(username, ts, forum, "post message", 0, 0, false);
        controller = new ThreadController();
        thread = new Thread(username, ts, forum, "thread message", slug, "Thread Title", 3);
        thread.setId(threadID);
    }

    @Nested
    class CheckSlugAndIdTests {
        @Test
        void givenSlug_shouldReturnThread() {
            try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
                threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
                assertEquals(controller.CheckIdOrSlug(slug), thread);
                assertNull(controller.CheckIdOrSlug("unknownSlug"));
            }
        }

        @Test
        void givenId_shouldReturnThread() {
            try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
                threadMock.when(() -> ThreadDAO.getThreadById(threadID)).thenReturn(thread);
                assertEquals(controller.CheckIdOrSlug(Integer.toString(threadID)), thread);
                assertNull(controller.CheckIdOrSlug("1234"));
            }
        }
    }

    @Nested
    class CreatePostTests {
        @Test
        void givenValidPost_shouldCreatePost() {
            try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class);
                    MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {

                threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
                userMock.when(() -> UserDAO.Info(username)).thenReturn(user);

                assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(List.of(post)),
                        controller.createPost(slug, List.of(post)));
                assertEquals(thread, ThreadDAO.getThreadBySlug(slug));
            }
        }
    }

    @Nested
    class PostsTests {
        @Test
        void givenValidThread_shouldReturnPosts() {
            try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
                threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);

                final List<Post> postList = List.of(post);

                threadMock.when(() -> ThreadDAO.getPosts(threadID, 1, 100, "flat", false)).thenReturn(postList);

                // Empty list of posts
                threadMock.when(() -> ThreadDAO.getPosts(threadID, 0, 100, "flat", false))
                        .thenReturn(Collections.emptyList());

                assertEquals(ResponseEntity.status(HttpStatus.OK).body(postList),
                        controller.Posts(slug, 1, 100, "flat", false));
                assertEquals(ResponseEntity.status(HttpStatus.OK).body(Collections.emptyList()),
                        controller.Posts(slug, 0, 100, "flat", false));
            }
        }
    }

    @Nested
    class ChangeThreadTests {
        @Test
        void givenValidChanges_shouldChangeThread() {
            try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
                final String slug2 = "anotherSlug";
                final Timestamp ts2 = Timestamp.valueOf("2022-02-21 19:45:00");
                final int threadID2 = 871999;
                final Thread thread2 = new Thread(username, ts2, "something", "some_message", slug2, "Title", 4);
                thread2.setId(threadID2);
                threadMock.when(() -> ThreadDAO.getThreadBySlug(slug2)).thenReturn(thread2);
                threadMock.when(() -> ThreadDAO.getThreadById(threadID2)).thenReturn(thread2);
                threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);

                threadMock.when(() -> ThreadDAO.change(thread, thread2)).thenAnswer(invocation -> {
                    threadMock.when(() -> ThreadDAO.getThreadBySlug(slug2)).thenReturn(thread2);
                    return null;
                });

                assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread2),
                        controller.change(slug2, thread2));
                assertEquals(thread2, controller.CheckIdOrSlug(slug2));
            }
        }
    }

    @Nested
    class ThreadInfoTests {
        @Test
        void givenValidThread_shouldReturnThreadDetails() {
            try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
                threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
                assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), controller.info(slug));

                // Non-existing thread
                threadMock.when(() -> ThreadDAO.getThreadBySlug("unknownSlug")).thenThrow(new DataAccessException("") {
                });
                assertEquals(ResponseEntity.status(HttpStatus.NOT_FOUND).body(null).getStatusCode(),
                        controller.info("unknownSlug").getStatusCode());
            }
        }
    }

    @Nested
    class CreateVoteTests {
        @Test
        void givenValidVote_shouldCreateVote() {
            try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
                try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                    userMock.when(() -> UserDAO.Info(username)).thenReturn(user);
                    final Vote vote = new Vote(username, 1);
                    threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
                    threadMock.when(() -> ThreadDAO.change(vote, thread.getVotes())).thenReturn(thread.getVotes());
                    assertEquals(ResponseEntity.ok(thread), controller.createVote(slug, vote));
                }
            }
        }
    }
}
